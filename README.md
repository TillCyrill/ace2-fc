# Enzyme inhbitors employ different mechanisms to stabilize broad-spectrum antiviral ACE2-Fc fusion proteins
##Authors
Hristo L. Svilenov 1*, Florent Delhommel 2,3, Till Siebermorgen 2,3, Florian Rührnößl 1, Grzegorz M Popowicz 2,3, Michael Sattler 2,3, Carsten Brockmeyer 4 and Johannes Buchner 1* \
1 Center of functional protein assemblies (CPA) and School of Natural Sciences, Department of Biosciences, Technical University of Munich, Garching, Germany.\
2 Institute of Structural Biology, Helmholtz Zentrum München, Neuherberg, Germany. \
3 Bavarian NMR Center, School of Natural Sciences, Department of Biosciences, Technical University of Munich, Garching, 85748, Munich, Germany. \
4 Formycon AG, Martinsried/Planegg, Germany.

## Structure

In the subsequent folders the intialization restart files and the last snapshots and the topology files are given. For the simulations the Amber20 software suite was used. 


